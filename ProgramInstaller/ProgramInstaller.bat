@echo off
title Program Installer

:: Check if winget is installed
where winget >nul 2>&1
if %errorlevel% equ 0 (
    echo Winget not installed
    pause
	powershell -Command "$progressPreference = 'silentlyContinue'"
	powershell -Command "Invoke-WebRequest -Uri https://aka.ms/getwinget -OutFile Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle"
	powershell -Command "Invoke-WebRequest -Uri https://aka.ms/Microsoft.VCLibs.x64.14.00.Desktop.appx -OutFile Microsoft.VCLibs.x64.14.00.Desktop.appx"
	powershell -Command "Invoke-WebRequest -Uri https://github.com/microsoft/microsoft-ui-xaml/releases/download/v2.8.6/Microsoft.UI.Xaml.2.8.x64.appx -OutFile Microsoft.UI.Xaml.2.8.x64.appx"
	powershell -Command "Add-AppxPackage Microsoft.VCLibs.x64.14.00.Desktop.appx"
	powershell -Command "Add-AppxPackage Microsoft.UI.Xaml.2.8.x64.appx"
	powershell -Command "Add-AppxPackage Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle"
	echo.
	echo finished install
	pause
)

:menu
cls
echo ------------------------------
echo [1] 7-Zip           [2] WinRAR
echo [3] Discord         [4] Steam
echo [5] Spotify         [6] Gimp
echo [7] Blender         [8] Chrome
echo ------------------------------

set /p "pchoice=Please type the program number you would like to install: "

if %pchoice%==1 goto 1
if %pchoice%==2 goto 2
if %pchoice%==3 goto 3
if %pchoice%==4 goto 4
if %pchoice%==5 goto 5
if %pchoice%==6 goto 6
if %pchoice%==7 goto 7
if %pchoice%==8 goto 8

echo Please choose a valid option (1-8)
echo Press any key to go back to the menu;
pause > nul
goto menu

:: 7-Zip
:1
cd %ProgramFiles%
if exist "7-Zip" (
    echo 7-Zip is already installed, installation will not continue.
	goto menuconfirm
)

echo 7-Zip is not installed on this computer, proceeding with download..
powershell -Command "Invoke-WebRequest -Uri 'https://7-zip.org/a/7z2406-x64.msi' -OutFile \"$([Environment]::GetFolderPath('MyDocuments'))\7z2406-x64.msi\""
echo Program has been downloaded, proceeding with installation..
msiexec.exe /i "%USERPROFILE%\Documents\7z2406-x64.msi" /qn
echo Program 7-Zip has been installed!
goto menuconfirm

:: WinRar
:2
cd %ProgramFiles%
if exist "WinRAR" (
	echo WinRAR is already installed, installation will not continue.
	goto menuconfirm
)

echo WinRAR is not installed on this computer, proceeding with download..
winget install -e --id RARLab.WinRAR
echo Program has been downloaded, starting installation..
cd %USERPROFILE%\Documents
start /wait winrar-x64-601.exe /S
echo Program has been installed!
goto menuconfirm

:: Discord
:3
cd %AppData%
if exist "Discord" (
    echo Discord is already installed, installation will not continue.
	goto menuconfirm
)

echo Discord is not installed on this computer, proceeding with download..
powershell -Command "Invoke-WebRequest -Uri 'https://discord.com/api/downloads/distributions/app/installers/latest?channel=stable&platform=win&arch=x64' -OutFile \"$([Environment]::GetFolderPath('MyDocuments'))\DiscordSetup.exe\""
echo Program has been downloaded, starting installation..
cd "%USERPROFILE%\Documents"
start /wait DiscordSetup.exe
echo Program has been installed!
goto menuconfirm

:: Steam
:4
cd %USERPROFILE%\AppData\Local
if exist "Steam" (
	echo Steam is already installed, installation will not continue.
	goto menuconfirm
)

echo Steam is not installed on this computer, proceeding with download..
powershell -Command "Invoke-WebRequest -Uri 'https://cdn.akamai.steamstatic.com/client/installer/SteamSetup.exe' -OutFile \"$([Environment]::GetFolderPath('MyDocuments'))\SteamSetup.exe\""
echo Program has been downloaded, starting installation..
cd %USERPROFILE%\Documents
start /wait SteamSetup.exe /S
echo Progam has been installed!
goto menuconfirm

:: Spotify
:5
cd %appdata%
if exist "Spotify" (
	echo Spotify is already installed, installation will not continue.
	goto menuconfirm
)

echo Spotify is not installed on this computer, proceeding with download..
powershell -Command "Invoke-WebRequest -Uri 'https://download.scdn.co/SpotifySetup.exe' -OutFile \"$([Environment]::GetFolderPath('MyDocuments'))\SpotifySetup.exe\""
cd %USERPROFILE%\Documents
start /wait SpotifySetup.exe /S
echo Program has been installed!
goto menuconfirm

:: Gimp
cd %AppData%
if exist "GIMP" (
	echo GIMP is already installed, installation will not continue.
	goto menuconfirm
)

echo GIMP is not installed on this computer, proceeding with download..
powershell -Command "Invoke-WebRequest -Uri 'https://download.gimp.org/gimp/v2.10/windows/gimp-2.10.38-setup.exe' -OutFile \"$([Environment]::GetFolderPath('MyDocuments'))\gimp-2.10.38-setup.exe\""
cd %USERPROFILE%\Documents
start /wait gimp-2.10-38-setup.exe /S
echo Program has been installed!
goto menuconfirm

:: Blender
cd %ProgramFiles%
if exist "Blender Foundation" (
	echo Blender is already installed, installation will not continue.
	goto menuconfirm
)

echo Blender is not installed on this computer, proceeding with download..
winget install -e --id BlenderFoundation.Blender -h --accept-package-agreements --accept-source-agreements




:menuconfirm
echo Press any key to go back to the menu.
pause > nul
goto menu