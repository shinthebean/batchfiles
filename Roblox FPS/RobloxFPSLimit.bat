:: MIT License

:: Copyright (c) 2019 axstin

:: Permission is hereby granted, free of charge, to any person obtaining a copy
:: of this software and associated documentation files (the "Software"), to deal
:: in the Software without restriction, including without limitation the rights
:: to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
:: copies of the Software, and to permit persons to whom the Software is
:: furnished to do so, subject to the following conditions:

:: The above copyright notice and this permission notice shall be included in all
:: copies or substantial portions of the Software.

:: THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
:: IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
:: FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
:: AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
:: LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
:: OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
:: SOFTWARE.

@echo off
setlocal enabledelayedexpansion

set "rootDir=%USERPROFILE%\AppData\Local\Roblox\Versions"
set "recentFolder="
set "recentDate="

:beforesetFPS
cls
set /p "fpsCap=Enter the FPS Cap (1-50000): "
set "fpsCap=!fpsCap: =!"

echo !fpsCap!| findstr /r "^[0-9][0-9]*$" >nul
if errorlevel 1 (
    echo FPS Cap value must be numerical.
    timeout 4 > nul
    goto beforesetFPS
)
set /a "fpsCap=fpsCap"

if !fpsCap! leq 0 (
    echo FPS cap must be greater than 0.
    echo Press any key to go back to the start.
    pause > nul
    goto beforesetFPS
)

if !fpsCap! gtr 50000 (
    echo FPS cap exceeds the maximum value of 50000.
    echo Press any key to go back to the start.
    pause > nul
    goto beforesetFPS
)
goto StartScript

:StartScript
for /f "delims=" %%D in ('dir /a:d /b /s /o:-d "%rootDir%" 2^>nul') do (
    set "folderDate=%%~tD" > nul
    if not defined recentDate (
        set "recentDate=!folderDate!"
        set "recentFolder=%%D"
    )
    if !folderDate! gtr !recentDate! (
        set "recentDate=!folderDate!"
        set "recentFolder=%%D"
    )
)

if not defined recentFolder (
    echo Failed to find a recent Roblox version folder.
    pause
    exit /b 1
)

if exist "!recentFolder!\ClientSettings" (
    del /q /f "!recentFolder!\ClientSettings"
    timeout 2 > nul
) else (
    goto :CreateFiles2
)

:CreateFiles2
if not exist "!recentFolder!\ClientSettings" (
    mkdir "!recentFolder!\ClientSettings" > nul 2>&1
    if errorlevel 1 (
        echo Failed to create ClientSettings folder.
        pause
        exit /b 1
    )
)

(
    echo {
    echo     "DFIntTaskSchedulerTargetFps": !fpsCap!
    echo }
) > "!recentFolder!\ClientSettings\ClientAppSettings.json"

if errorlevel 1 (
    echo Failed to create or update ClientAppSettings.json.
) else (
    echo Successfully set FPS Cap to !fpsCap!
    echo Restart Roblox in order for changes to apply.
)

pause > nul
