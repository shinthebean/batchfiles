:: MIT License

:: Copyright (c) 2024 ShinTheBean

:: Permission is hereby granted, free of charge, to any person obtaining a copy
:: of this software and associated documentation files (the "Software"), to deal
:: in the Software without restriction, including without limitation the rights
:: to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
:: copies of the Software, and to permit persons to whom the Software is
:: furnished to do so, subject to the following conditions:

:: The above copyright notice and this permission notice shall be included in all
:: copies or substantial portions of the Software.

:: THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
:: IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
:: FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
:: AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
:: LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
:: OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
:: SOFTWARE.

@echo off
echo                           Created by shinthebean
echo                    for PC Help Hub Discord and General Use
echo                   https://gitlab.com/shinthebean/batchfiles
echo.
echo.


for /f "usebackq tokens=*" %%A in (`powershell -command "hostname"`) do (
    set "COMPUTER_NAME=%%A"
)

echo ----------------------------------------
echo     GATHERING SPECS from %COMPUTER_NAME%
echo -----------------------------------------
echo.

:: CPU
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-CimInstance Win32_Processor).Name"`) do (
    set "CPU=%%A"
)

:: GPU
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-CimInstance Win32_VideoController).Caption"`) do (
    set "GPU=%%A"
)

:: RAM Total
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-CimInstance Win32_PhysicalMemory | Measure-Object -Property Capacity -Sum).Sum / 1GB"`) do (
    set "TOTAL_PHYSICAL_MEMORY=%%A GB"
)

:: Motherboard
for /F "usebackq tokens=*" %%A in (`powershell -command "Get-WmiObject Win32_BaseBoard | Select-Object -ExpandProperty Product"`) do (
    set "MOTHERBOARD=%%A"
)

:: BIOS Type
for /f "usebackq tokens=*" %%A in (`powershell -command "Get-WmiObject Win32_ComputerSystem | ForEach-Object { if ($_.BootMode -eq 1) { 'Legacy' } else { 'UEFI' }}"`) do (
	set "BIOSMode=%%A"
)

:: BIOS Version
for /f "usebackq tokens=*" %%A in (`powershell -command "Get-WmiObject Win32_BIOS | Select-Object -ExpandProperty SMBIOSBIOSVersion"`) do (
    set "BIOS=%%A"
)

:: BIOS Date
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-WmiObject Win32_BIOS).ReleaseDate | ForEach-Object { [datetime]::ParseExact($_.Substring(0,8), 'yyyyMMdd', $null).ToString('MM/dd/yyyy') }"`) do (
    set "BIOS_DATE=%%A"
)

:: OS Version
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-CimInstance Win32_OperatingSystem).Caption"`) do (
    set "OS_VERSION=%%A"
)

:: Secure Boot
for /f "usebackq tokens=*" %%A in (`powershell -command "if ((Get-ItemProperty -Path 'HKLM:\\SYSTEM\\CurrentControlSet\\Control\\SecureBoot\\State' -Name UEFISecureBootEnabled).UEFISecureBootEnabled -eq 1) { 'Enabled' } else { 'Disabled' }"`) do ( :: Checks registry for secureboot state, if value = 0 it shows as diabled; vise versa.
    set "SECURE_BOOT=%%A"
)

:: Output to Terminal
echo Computer Name: %COMPUTER_NAME%
echo CPU: %CPU%
echo GPU: %GPU%
echo RAM Installed: %TOTAL_PHYSICAL_MEMORY%
echo.
echo Motherboard: %MOTHERBOARD%
echo BIOS Version: %BIOS% %BIOS_DATE%
echo BIOS Mode: %BIOSMode%
echo.
echo OS Version: %OS_VERSION%
echo Secure Boot: %SECURE_BOOT%
echo.
echo Moving Output file to Desktop..

:OutputFile
:: Output to File (specs_list.txt)
(
    echo Computer Name: %COMPUTER_NAME%
    echo CPU: %CPU%
    echo GPU: %GPU%
	echo RAM Installed: %TOTAL_PHYSICAL_MEMORY%
    echo Motherboard: %MOTHERBOARD%
    echo BIOS Version: %BIOS% %BIOS_DATE%
	echo BIOS Mode: %BIOSMode%
    echo OS Version: %OS_VERSION%
	echo Secure Boot: %SECURE_BOOT%
) > specs_list.txt

:: Define variables
set "filename=specs_list.txt"
set "desktopfolder=%USERPROFILE%\Desktop"

:: Search for output.txt; move to desktop
powershell -command "Get-ChildItem -Path C:\ -Filter %filename% -Recurse -ErrorAction SilentlyContinue | ForEach-Object { Move-Item -Path $_.FullName -Destination '%desktopfolder%' -Force }"
echo specs_list.txt moved to your Desktop..
echo If file not found on desktop, rerun the batch file.


pause
timeout 5 > nul
exit
