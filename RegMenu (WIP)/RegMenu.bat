@echo off
setlocal enabledelayedexpansion
title Registry Menu

:menu
echo Registry Menu
echo -------------
echo [1] Check for Registry Backup
echo [2] Create a Registry Backup
echo [3] Import a Registry Backup
echo [4] Rewrite .DLL's to Registry
echo [5] Exit

set /p choice="Select an option: "
if "%choice%"=="1" goto RegCheck
if "%choice%"=="2" goto CreateBackup
if "%choice%"=="3" goto ImportBackup
if "%choice%"=="4" goto RewriteReg
if "%choice%"=="5" exit

:RegCheck
echo Checking for Registry Backup...
rem wip
goto menu

:CreateBackup
mkdir "%USERPROFILE%\Documents\Registry Backups" >nul 2>&1

if exist "%USERPROFILE%\Documents\Registry Backups" (
    set regpath="%USERPROFILE%\Documents\Registry Backups"
    goto startbackup
) else (
    echo Script has failed to make the "Registry Backups" folder in %USERPROFILE%\Documents
    set /p "1retryprompt=Would you like to retry? [Y/N]: "
    if /i "%1retryprompt%"=="Y" (
        goto :CreateBackup
    ) else if /i "%1retryprompt%"=="N" (
        cls
        goto menu
    )
)
echo Please select Y or N
goto CreateBackup

:startbackup
set /p "regcreatename=What would you like to name your Registry Backup? "
reg export HKLM "%USERPROFILE%\Documents\Registry Backups\%regcreatename%.reg"
echo Registry Backup created in %USERPROFILE%\Documents\Registry Backups
echo Press any key to go back to Menu
pause > nul
goto menu

:ImportBackup
echo Searching for Backups (.reg) in Documents folder...
set "excludeDir=%windir%"
2>nul dir /s /b /a-d "%USERPROFILE%\Documents\*.reg" | findstr /v /i "%excludeDir%" > RegResults.txt

rem checks if any content in RegResults.txt
for %%A in (RegResults.txt) do set size=%%~zA
if %size% equ 0 (
    echo No backups have been found!
    echo Press any key to go back to the menu.
    pause > nul
    goto menu
)

set "counter=0"
set "last="
set "files=()"
for /f "tokens=* delims=" %%a in (RegResults.txt) do (
    set "line=%%a"
    if /i "!line!" neq "!last!" (
        set /a counter+=1
        set "files[!counter!]=%%a"
        echo [!counter!] %%a
    )
    set "last=%%a"
)

set /p regchoice="Choose which .reg to use as the Import: "

if not defined files[%regchoice%] (
    echo The choice you have selected isn't valid, please refer to the number listed on the .reg's listed above!
    goto ImportBackup
)

set regfile=!files[%regchoice%]!
echo You have chosen: %regfile%
goto YNRestore

:YNRestore
echo It is highly recommended to create a Restore Point before proceeding.
set /p "restoreyn=Would you like to create a Restore Point [Y/N]: "

if /i "%restoreyn%"=="Y" (
    goto YesRestore
) else if /i "%restoreyn%"=="N" (
    goto importregstart
)

echo Please select Y or N.
goto YNRestore

:YesRestore
echo.
set /p restorename="Please choose a name for the restore point: "
set restoreDesc=%restorename%
set reg_key=HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\SystemRestore
reg add "%reg_key%" /v "SystemRestorePointCreationFrequency" /t REG_DWORD /d 01 /f >nul 2>&1
timeout 1 > nul
powershell -Command "Checkpoint-Computer -Description \"%restoreDesc%\" -RestorePointType \"MODIFY_SETTINGS\"" >nul 2>&1
reg delete "%reg_key%" /v "SystemRestorePointCreationFrequency" /f >nul 2>&1
echo Restore Point Created!
pause > nul

:importregstart
echo Importing registry backup...
reg import "%regfile%"
echo Registry backup imported!
pause > nul
goto menu

:RewriteReg
echo Rewriting .DLL's into registry...
for /f %%s in ('dir /b *.dll') do regsvr32 /s %%s >nul 2>&1
echo Rewrite finished!
echo Press any key to restart your computer for changes to apply.
pause > nul
shutdown /r /t 0
@echo off
setlocal enabledelayedexpansion
title Registry Menu

:menu
echo Registry Menu
echo -------------
echo [1] Check for Registry Backup
echo [2] Create a Registry Backup
echo [3] Import a Registry Backup
echo [4] Rewrite .DLL's to Registry
echo [5] Exit

set /p choice="Select an option: "
if "%choice%"=="1" goto RegCheck
if "%choice%"=="2" goto CreateBackup
if "%choice%"=="3" goto ImportBackup
if "%choice%"=="4" goto RewriteReg
if "%choice%"=="5" exit

:RegCheck
echo Checking for Registry Backup...
rem wip
goto menu

:CreateBackup
mkdir "%USERPROFILE%\Documents\Registry Backups" >nul 2>&1

if exist "%USERPROFILE%\Documents\Registry Backups" (
    set regpath="%USERPROFILE%\Documents\Registry Backups"
    goto startbackup
) else (
    echo Script has failed to make the "Registry Backups" folder in %USERPROFILE%\Documents
    set /p "1retryprompt=Would you like to retry? [Y/N]: "
    if /i "%1retryprompt%"=="Y" (
        goto :CreateBackup
    ) else if /i "%1retryprompt%"=="N" (
        cls
        goto menu
    )
)
echo Please select Y or N
goto CreateBackup

:startbackup
set /p "regcreatename=What would you like to name your Registry Backup? "
reg export HKLM "%USERPROFILE%\Documents\Registry Backups\%regcreatename%.reg"
echo Registry Backup created in %USERPROFILE%\Documents\Registry Backups
echo Press any key to go back to Menu
pause > nul
goto menu

:ImportBackup
echo Searching for Backups (.reg) in Documents folder...
set "excludeDir=%windir%"
2>nul dir /s /b /a-d "%USERPROFILE%\Documents\*.reg" | findstr /v /i "%excludeDir%" > RegResults.txt

rem checks if any content in RegResults.txt
for %%A in (RegResults.txt) do set size=%%~zA
if %size% equ 0 (
    echo No backups have been found!
    echo Press any key to go back to the menu.
    pause > nul
    goto menu
)

set "counter=0"
set "last="
set "files=()"
for /f "tokens=* delims=" %%a in (RegResults.txt) do (
    set "line=%%a"
    if /i "!line!" neq "!last!" (
        set /a counter+=1
        set "files[!counter!]=%%a"
        echo [!counter!] %%a
    )
    set "last=%%a"
)

set /p regchoice="Choose which .reg to use as the Import: "

if not defined files[%regchoice%] (
    echo The choice you have selected isn't valid, please refer to the number listed on the .reg's listed above!
    goto ImportBackup
)

set regfile=!files[%regchoice%]!
echo You have chosen: %regfile%
goto YNRestore

:YNRestore
echo It is highly recommended to create a Restore Point before proceeding.
set /p "restoreyn=Would you like to create a Restore Point [Y/N]: "

if /i "%restoreyn%"=="Y" (
    goto YesRestore
) else if /i "%restoreyn%"=="N" (
    goto importregstart
)

echo Please select Y or N.
goto YNRestore

:YesRestore
echo.
set /p restorename="Please choose a name for the restore point: "
set restoreDesc=%restorename%
set reg_key=HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\SystemRestore
reg add "%reg_key%" /v "SystemRestorePointCreationFrequency" /t REG_DWORD /d 01 /f >nul 2>&1
timeout 1 > nul
powershell -Command "Checkpoint-Computer -Description \"%restoreDesc%\" -RestorePointType \"MODIFY_SETTINGS\"" >nul 2>&1
reg delete "%reg_key%" /v "SystemRestorePointCreationFrequency" /f >nul 2>&1
echo Restore Point Created!
pause > nul

:importregstart
echo Importing registry backup...
powershell -Command "Start-Process powershell -ArgumentList 'reg import \"%regfile%\"' -Verb RunAs"
echo Registry backup imported!
pause > nul
goto menu

:RewriteReg
echo Rewriting .DLL's into registry...
for /f %%s in ('dir /b *.dll') do regsvr32 /s %%s >nul 2>&1
echo Rewrite finished!
echo Press any key to restart your computer for changes to apply.
pause > nul
shutdown /r /t 0
